package battle;

import java.util.ArrayList;
import java.util.List;

public class Battlefield {
    List<Ship> ships;

    Battlefield() {
        ships = new ArrayList<>(10);
        ConsoleReader reader = new ConsoleReader();
        int sh=0;
        while (sh <= 3) {
            Ship ship = reader.getShip();
            if (ship.decks.size() == 1) {
                sh++;
                ships.add(ship);
            }
        }
    }
}
