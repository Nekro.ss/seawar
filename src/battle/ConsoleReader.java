package battle;
import java.util.Scanner;

public class ConsoleReader {
    Scanner skl = new Scanner(System.in);

    public Coordinate getCoordinate() {
        System.out.print("Произведите выстрел: ");
        String shot = skl.nextLine();
        shot = shot.substring(0, 1).toUpperCase() + shot.substring(1);
        char letter = shot.charAt(0);
        int x;
        x = letter - 'A';
        int y = Integer.parseInt(shot.substring(1));
        Coordinate coordinate = new Coordinate(x, y);
//        System.out.println("Выстрел: " + " " + x + " " + y);
        return coordinate;
    }

    public Ship getShip() {
        System.out.println("Введите координаты коробля: ");
        String ships = skl.nextLine().toUpperCase();
        char letter = ships.charAt(0);
        int x;
        x = letter - 'A';
        int y = Integer.parseInt(ships.substring(1));
        System.out.println("x" + x + " " + "y" + y);
        Boolean isHorizontal = null;
        while (isHorizontal==null) {
            System.out.println("Введите расположение коробля: ");
            String horizontal = skl.nextLine();
            if (horizontal.equalsIgnoreCase("h")) {
                isHorizontal = true;
            } else if (horizontal.equalsIgnoreCase("v")) {
                isHorizontal = false;
            } else {
                System.out.println("Введите h или v");
            }
        }
            System.out.println("Расположение: " + isHorizontal);
        System.out.println("Введите размер коробля: ");
        int size = skl.nextInt();
        System.out.println("size: " + size);
        Ship ship = new Ship(size, new Coordinate(x, y), isHorizontal);
        return ship;
    }
}




